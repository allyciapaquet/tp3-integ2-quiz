// LA LISTE DES QUESTIONS 

let donneesEntrantes = `
[
	{
		"question":"En quel année le Titanic à coulé ?",
		"reponses":["Il n'a jamais coulé","En 1940","En 1912"], 
		"reponse":0,
		"bonneReponse": "En 1912"
	},
	{
		"question":"Quel est l'anime le plus regardé ?",
		"reponses":["One Piece","Naruto","Attack on Titan"], 
		"reponse":0,
    "bonneReponse": "One Piece"
	},
    {
		"question":"Quel couleur fait le mélange de bleu et rouge ?",
		"reponses":["Vert","Violet","Orange"], 
		"reponse":1,
    "bonneReponse": "Violet"
	},
    {
		"question":"Quel est le prénom féminin le plus donné au monde ?",
		"reponses":["Roxanne","Sarah","Sofia"], 
		"reponse":2,
    "bonneReponse": "Sofia"
	},
    {
		"question":"Quelle main est la plus forte au poker ?",
		"reponses":["Quinte Flush Royale","Full","Double Paire"], 
		"reponse":0,
    "bonneReponse": "Quinte Flush Royale"
	}	
]
`
// CONSTANTES, TABLEAU ET AUTRES

const donnees = {}
const questions = JSON.parse(donneesEntrantes)
let questionCourante = 0
donnees.reponses = []
$('#tableau').hide(),$('#accordeon').hide(),$('#modal1').hide(),$('#modal2').hide()


// VALIDATION DES INFO DU FORMULAIRE 

$(function ajouterValidation() { 
	$('#questionnaire').validate({
		rules: {
			prenom: "required",
			nom: "required",
			date_naissance: "required",
			choix: "required",

		},
		messages: { 
			prenom: "Veuillez entrer un prénom",
			nom: "Veuillez entrer un nom",
			naissance: "La date de naissance est requise"
		},
		submitHandler: function(){
			donnees.prenom = $('#prenom').val()
			$("#quiz").html('');
			CreerQuiz();
		},
        showErrors: function(errorMap, errorList) {
        let est_soumis = true;
        if (est_soumis) {
            var sommaire = "Vous avez les erreurs: \n";
            $.each(errorList, function() { sommaire += " * " + this.message + "\n"; });
            est_soumis = false;
        }
        this.defaultShowErrors();
        },
        invalidHandler: function(form, validator) {
          est_soumis = true;
    
        }})});

	

	function CreerQuiz(){
		afficherQuestionQuiz()
	}

	function afficherQuestionQuiz(){
		const section = $('#quiz')
		section.html('')
		question = questions[questionCourante]
		section.append('<h1>' + question.question + '</h1>')
		.attr('class', 'border')
		$('h1').attr('class', 'font-weight-bold')
		for(let i = 0; i < question.reponses.length; i++) {
			
			const reponse = $('<input></input>')
			reponse.attr('type', 'radio')
			       .attr('name', 'question')
			       .attr('id','radio'+1)
			       .attr('value', i)
				   
			const libelle = $('<label>' + question.reponses[i] + '</label>')
			libelle.attr("for","radio"+1)
			const appercu = $('<fieldset></fieldset>')
			section.append(appercu)
			appercu.append(reponse)
			appercu.append(libelle)	
		  }
		  const bouton = $('<button>Soumettre question</button>').css('background-color','pink')
		  bouton.attr('id', 'suivante')
		  bouton.on('click', function(){
			const reponse = $('input[name=question]:checked').val()
			questions[questionCourante].reponseFournie	= parseInt(reponse)
			questionCourante += 1
			if(questionCourante >= questions.length){
					afficherRapport()
		 		}
				else{
					afficherQuestionQuiz()
				}
		})
        $('#quiz').append(bouton)
	}

function afficherRapport () {
	$('#quiz').html('')
	$('#tableau').DataTable();
	$('#tableau').show()
	console.log(questions)
	for (let i = 0; i < questions.length; i++) {
        let questionCourante = questions[i]
        const tr = $('<tr></tr>')
        tr.append('<td>' + (i + 1) + '</td>') 
        tr.append('<td>' + questionCourante.question + '</td>')
        if (questionCourante.reponse === questionCourante.reponseFournie) {
          tr.append('<td>' + 'crochet' + '</td>') 
		  $("#modal1").modal()
        } else {
          tr.append('<td>X</td>') 
		  $("#modal2").modal({
			fadeDuration: 500
		  })
        }
        $('tbody').append(tr)
      }
	    $('#accordeon').show().css('border','solid 1 px black')
	    $( function() {
		  $( "#accordeon").accordion({
		    collapsible: true
		  });
	    });

	    for(let j = 0; j < questions.length; j++){
		  question = questions[j]
		  $('<h3>' + [j + 1] + question.question + '</h3>').appendTo(accordeon)
		  const div = $('<div></div>')
		  $('<p>' + 'La bonne réponse est : ' + question.bonneReponse + '</p>').appendTo(div)
		  div.appendTo(accordeon)
		  }
	}

function bonneReponse(){
  if(questionCourante.reponse === questionCourante.reponseFournie){
    return true 
  }
  bonneReponse =+1
}

// JQUERY VALIDATOR 

//https://www.pierrefay.fr/blog/jquery-validate-formulaire-validation-tutoriel.html
$.validator.addMethod(
  "regex",
  function (value, element, regexp) {
    if (regexp.constructor != RegExp)
      regexp = new RegExp(regexp);
    else if (regexp.global)
      regexp.lastIndex = 0;
    return this.optional(element) || regexp.test(value);
  }, "erreur expression reguliere"
);

jQuery.validator.addMethod("alphanumeric", function (value, element) {
  return this.optional(element) || /^[\w.]+$/i.test(value);
}, "Letters, numbers, and underscores only please");


